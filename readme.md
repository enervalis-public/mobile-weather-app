# The Weather Station
## The Concept

For this assignment, you will build a mobile application. The app shows the weatherforecast for the user's current location. It show the forecast for today, but also for the next 5 days.
The purpose is to showcase your programming skills. While making the assignement, we will be looking at the following aspects:

* Readability and maintainability of your code (Clean code)
* Testing your code (e.g. unit tests)
* Design of your application (e.g. architecture, patterns)

## The Assignment

At the end of this exercise, you should demonstrate your application, ready to run on a device. You are free to choose IOS or android, and can use any libraries or frameworks you like. We do want a native application, so no webviews or hybrid apps.

The application consists out of a single screen, showing the weather forecast of your current location. The forecast shows a description and an icon depicting the current status, the current temperature as well as the minimum and maximum temperature for today. All temperatures should be shown in degrees celcius.

This screen also shows the five day forecast, with the name of the day, an icon summarizing the weather conditions and the expected minimum and maximum temperature for the day.

In designs folder, you will find a screenshot of the app. This is just a suggestion, you are free to design the app as you like. 

If you're not connected to the internet, or if there is a problem reaching the API, the user should be notified of this.


## The data

To get to the weather data, please use (https://openweathermap.org/")[https://openweathermap.org/].

For today's forecast, use (https://openweathermap.org/current")[https://openweathermap.org/current], and for the five day forecast, use (https://openweathermap.org/forecast5")[https://openweathermap.org/forecast5]

Use the info provided at (https://openweathermap.org/weather-conditions)[https://openweathermap.org/weather-conditions] to make the app visually attractive. You can load the weather icons from the OpenWeatherMap site, or use the ones provided in the images folder.

Use the following API key: openweathermap-key: e5f670fdf8c2ffde3c546ffe925fec22

**BE CAREFUL**
The API key that we use is a free key. The free key is limited to 60calls/minute. If you exceed this, the API key will be blocked for some amount of time. Write your code carefully and always test first before running it with the actual API.
